set -e
set -x

n=windows-default-manifest
eval v=$(awk '/^Version/{print $2}' cygwin-default-manifest.spec)

git clone -b release-${v/./_} git://sourceware.org/git/cygwin-apps/windows-default-manifest.git

mv $n $n-$v
tar --exclude-vcs -Jcf $n-$v.tar.xz $n-$v/
rm -fr $n-$v/
